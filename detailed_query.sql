
CREATE OR REPLACE FUNCTION func_rate_to_usd(
  currency_id BIGINT,
  balance_update TIMESTAMP
) RETURNS NUMERIC AS $$
DECLARE
  rate NUMERIC DEFAULT NULL;
BEGIN
  SELECT rate_to_usd INTO rate
  FROM currency
  WHERE id = currency_id AND updated <= balance_update
  ORDER BY updated DESC
  LIMIT 1;
  IF (rate IS NULL) THEN
    SELECT rate_to_usd INTO rate
    FROM currency
    WHERE id = currency_id
    ORDER BY updated
    LIMIT 1;
  END IF;
  
  RETURN rate;
END;
$$ LANGUAGE plpgsql;


SELECT COALESCE("user".name, 'not defined') as name, 
       COALESCE("user".lastname, 'not defined') as lastname,
       (SELECT name FROM currency WHERE id = currency_id LIMIT 1) as currency_name,
       balance.money * func_rate_to_usd(currency_id, balance.updated) as currency_in_usd
FROM balance
LEFT JOIN "user" ON balance.user_id = "user".id
WHERE currency_id IN (SELECT id FROM currency)
ORDER BY name DESC, lastname, currency_name
;

DROP FUNCTION IF EXISTS func_rate_to_usd;


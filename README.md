# Data Warehouse

This project is a demonstration of Data Warehouse system.

`classic_dwh.sql` contains an SQL statement that returns the total volume (sum of all money) of transactions from user balance aggregated by user and balance type. All data (including data with anomalies) is processed.

`detailed_query.sql` contains an SQL statement that returns all Users, all Balance transactions (currencies that do not have a key in the Currency table are ignored) with currency name and calculated value of currency in USD for the nearest day.

![clarification](materials/detailed_query_clarification.png)

- finds nearest rate_to_usd of currency at the past (t1) 
- if t1 is empty (means no any rates at the past) then finds nearest rate_to_usd of currency at the future (t2)
- uses t1 OR t2 rate to calculate a currency in USD format

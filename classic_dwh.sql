WITH volume AS
	(
		SELECT user_id, 
			   type, 
			   sum(money) AS volume, 
			   currency_id
		FROM balance
		GROUP BY user_id, type, currency_id
		ORDER BY user_id, type
	),
	last_course AS
	(
		SELECT *
		FROM currency
		WHERE updated = (SELECT max(updated) FROM currency)
	), 
	volume_in_currency AS
	(
		SELECT user_id, 
			   type, 
			   volume, 
			   COALESCE(name, 'not defined') AS currency_name, 
			   COALESCE(rate_to_usd, 1) AS last_rate_to_usd
		FROM volume
		LEFT JOIN last_course lc ON currency_id = lc.id
	),
	users AS
	(
		SELECT COALESCE(u.name, 'not defined') AS name,
			   COALESCE(u.lastname, 'not defined') AS lastname,
			   type, 
			   volume, 
			   currency_name,
			   last_rate_to_usd
		FROM "user" u
		RIGHT JOIN volume_in_currency vic ON u.id = vic.user_id
	)
SELECT us.*, 
	   volume * last_rate_to_usd AS total_volume_in_usd
FROM users us
ORDER BY name DESC, lastname, type;